package com.platformbuilders.devtest.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.platformbuilders.devtest.dto.ClientDto;
import com.platformbuilders.devtest.model.Client;
import com.platformbuilders.devtest.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import java.util.List;

@Service
public class ClientService {

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private ClientRepository clientRepository;

    public ClientDto getClient(long clientId) {
        return new ClientDto(clientRepository.findById(clientId).orElseThrow(NotFoundException::new));
    }

    public Page<ClientDto> getClients(Pageable pageable, String name, String cpf) {
        if (name != null && cpf != null) {
            return ClientDto.convert(clientRepository.findByNameAndCpf(pageable, name, cpf));
        } else if (name != null || cpf != null) {
            return ClientDto.convert(clientRepository.findByNameOrCpf(pageable, name, cpf));
        } else {
            return ClientDto.convert(clientRepository.findAll(pageable));
        }
    }

    public ClientDto createClient(Client client) {
        return new ClientDto(clientRepository.save(client));
    }

    public List<ClientDto> createClients(List<Client> client) {
        return ClientDto.convert(clientRepository.saveAll(client));
    }

    public void deleteClient(long clientId) {
        clientRepository.deleteById(clientId);
    }

    public ClientDto updateClient(Client updatedClient, long clientId) {
        Client clientToBeUpdated = clientRepository.findById(clientId).orElseThrow(NotFoundException::new);

        clientToBeUpdated
                .withName(updatedClient.getName())
                .withCpf(updatedClient.getCpf())
                .withBirthday(updatedClient.getBirthday());

        return new ClientDto(clientRepository.save(clientToBeUpdated));
    }

    public ClientDto mergeUpdateClient(Client updatedClient, long clientId) {
        Client clientToBeUpdated = clientRepository.findById(clientId).orElseThrow(NotFoundException::new);

        if (updatedClient.getName() != null) {
            clientToBeUpdated.setName(updatedClient.getName());
        }
        if (updatedClient.getCpf() != null) {
            clientToBeUpdated.setCpf(updatedClient.getCpf());
        }
        if (updatedClient.getBirthday() != null) {
            clientToBeUpdated.setBirthday(updatedClient.getBirthday());
        }

        return new ClientDto(clientRepository.save(clientToBeUpdated));
    }

    public ClientDto patchUpdateClient(JsonPatch clientPatch, long clientId) throws JsonPatchException,
            JsonProcessingException {
        Client client = clientRepository.findById(clientId).orElseThrow(NotFoundException::new);
        Client clientPatched = applyPatchToCustomer(clientPatch, client);
        return new ClientDto(clientRepository.save(clientPatched));
    }

    private Client applyPatchToCustomer(JsonPatch clientPatch, Client targetClient) throws JsonPatchException,
            JsonProcessingException {
        JsonNode patched = clientPatch.apply(objectMapper.convertValue(targetClient, JsonNode.class));
        return objectMapper.treeToValue(patched, Client.class);
    }
}
