package com.platformbuilders.devtest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.platformbuilders.devtest.dto.ClientDto;
import com.platformbuilders.devtest.model.Client;
import com.platformbuilders.devtest.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.ws.rs.NotFoundException;
import java.net.URI;
import java.util.List;

@Validated
@RestController
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/client")
    public ResponseEntity<ClientDto> createClient(@RequestBody @Valid Client client,
                                                  UriComponentsBuilder uriBuilder) {
        ClientDto createdClient = clientService.createClient(client);

        URI uri = uriBuilder.path("/client/{id}").buildAndExpand(createdClient.getId()).toUri();
        return ResponseEntity.created(uri).body(createdClient);
    }

    @PostMapping("/clients")
    public ResponseEntity<List<ClientDto>> createClients(@RequestBody List<@Valid Client> clients) {
        return ResponseEntity.created(null).body(clientService.createClients(clients));
    }

    @GetMapping("/client/{clientId}")
    public ResponseEntity<ClientDto> getClient(@PathVariable long clientId) {
        return ResponseEntity.ok(clientService.getClient(clientId));
    }

    @GetMapping("/client")
    public ResponseEntity<Page<ClientDto>> getClients(@PageableDefault(page = 0, size = 5) Pageable pageable,
                                                      @RequestParam(required = false) String name,
                                                      @RequestParam(required = false) String cpf) {
        Page<ClientDto> clients = clientService.getClients(pageable, name, cpf);
        return ResponseEntity.ok(clients);
    }

    @PutMapping("/client/{clientId}")
    public ResponseEntity<ClientDto> updateClient(@RequestBody @Valid Client updatedClient,
                                                  @PathVariable long clientId) {
        try {
            return ResponseEntity.ok(clientService.updateClient(updatedClient, clientId));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(path = "/client/{clientId}", consumes = "application/merge-patch+json")
    public ResponseEntity<ClientDto> mergeUpdateClient(@RequestBody Client updatedClient,
                                                       @PathVariable long clientId) {
        try {
            return ResponseEntity.ok(clientService.mergeUpdateClient(updatedClient, clientId));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(path = "/client/{clientId}", consumes = "application/json-patch+json")
    public ResponseEntity<ClientDto> patchUpdateClient(@PathVariable long clientId,
                                                       @RequestBody JsonPatch clientPatch) {
        try {
            return ResponseEntity.ok(clientService.patchUpdateClient(clientPatch, clientId));
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (JsonPatchException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/client/{clientId}")
    public ResponseEntity<?> deleteClient(@PathVariable long clientId) {
        clientService.deleteClient(clientId);
        return ResponseEntity.noContent().build();
    }
}
