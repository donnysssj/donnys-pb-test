package com.platformbuilders.devtest.dto;

import com.platformbuilders.devtest.model.Client;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

public class ClientDto {

    private long id;
    private String name;
    private String cpf;
    private String birthday;
    private long age;

    public ClientDto() {
    }

    public ClientDto(Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.cpf = client.getCpf();
        this.birthday = client.getBirthday();
        this.age = getAge(client.getBirthday());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    private long getAge(String birthday) {
        return ChronoUnit.YEARS.between(LocalDate.now(), LocalDate.parse(birthday));
    }

    public static List<ClientDto> convert(List<Client> clients) {
        return clients.stream().map(client -> new ClientDto(client)).collect(Collectors.toList());
    }

    public static Page<ClientDto> convert(Page<Client> clients) {
        return clients.map(ClientDto::new);
    }
}
