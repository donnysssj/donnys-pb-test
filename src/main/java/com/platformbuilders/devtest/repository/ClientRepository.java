package com.platformbuilders.devtest.repository;

import com.platformbuilders.devtest.model.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Page<Client> findByNameOrCpf(Pageable pageable, String name, String cpf);
    Page<Client> findByNameAndCpf(Pageable pageable, String name, String cpf);
}
